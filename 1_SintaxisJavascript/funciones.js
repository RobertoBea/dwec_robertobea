function calcularVolumenSeparado(radio, alto) {
    // para calcular el volumen la formula es: V = (1/3)*Pi*radioAlCuatrado*Alto
    var volumen, areaBase, pi;
    pi = Math.PI;
    areaBase = pi * Math.pow(radio, 2);
    volumen = (1 / 3) * areaBase * alto;
    return volumen;
}
function calcularVolumenUnaLinea(radio, alto) {
    return (1 / 3) * Math.PI * Math.pow(radio, 2) * alto;
}
function primerPresidente(){
    var respuesta;
    do{
        respuesta = prompt("¿escribe el apellido del primer presidente de la democracia española?").toUpperCase();
    }while(respuesta != "SUAREZ")
}
function primerPresidente2(){
    var respuesta, intento = 1, pregunta = "¿Cuál fue el primer presidente de la democracia española?";
    do{
        if(intento==1){
            respuesta = prompt(pregunta).toUpperCase();
        }else{
            if (respuesta.substr(0,6)=="ADOLFO"){
                respuesta = prompt("Te falta el apellido, " + pregunta).toUpperCase();
            }else{
                respuesta = prompt("ERROR: Inténtelo de nuevo. " + pregunta).toUpperCase();
            }
        }
        intento++;
    }while(respuesta != "ADOLFO SUAREZ")
}
/*Crea una función que te permita mostrar los números del 1 al 100 (por pantalla, no utilizando un alert) excepto los múltiplos de 3.
¿Sabrías hacerlo de dos maneras diferentes?
Crea  otra función que muestre una lista con los meses del año excepto los meses de verano.
¿Sabrías hacerlo de dos maneras diferentes?
¡No olvides comentar el código!*/
var multiploDe = function(numero,multiplo){return numero%multiplo==0;}//creamos una funcion para calcular multiplos
function unoACienMenosMultiplos3(){
    for (var i=1;i<=100;i++){//con un bucle for recorremos los numeros del 1 al 100
        if (multiploDe(i,3)) continue;//si encontramos algun multiplo de 3 saltamos al siguiente registro
        document.write(i+",");//escribimos el numero
    }
}
function unoACienMenosMultiplos3B(){
    var num=0;
    do{//bucle do...while con la condicion de que el numero sea menos o igual a 100
        num++;//sumamos en cada pasada
        if (multiploDe(num,3)) continue;//si encontramos algun multiplo de 3 saltamos al siguiente registro
        document.write(num+",");//escribimos el numero
    }while(num<=100)
}
var esVerano = function(mes){return "6,7,8,9".includes(mes);}//creamos funcion que nos indica si es verano
function mesesAnyoMenosVerano(){
    for (var mes=1;mes<=12;mes++){//con un bucle for recorremos los meses del año
        if (esVerano(mes)) continue;//si encontramos algun mes de verano saltamos al siguiente registro
        document.write(mes+",");//escribimos el mes
    }
}
function mesesAnyoMenosVeranoB(){
    var mes=0;
    do{//bucle do...while con la condicion de que el numero sea menos o igual a 100
        mes++;//sumamos en cada pasada
        switch(mes){
            case 1:case 2:case 3:case 4:case 5:case 10:case 11:case 12: document.write(mes+","); break;//escribimos el mes
            default:
        }
    }while(mes<=12)
}
/* u1eFinal: Si has jugado alguna vez al trivial sabrás que, cuando un jugador consigue los 6 "quesitos", debe ir al centro del tablero y adivinar al menos 4 de las 6 preguntas que tiene una de las tarjetas escogida al azar.
Deberás realizar un programa que implemente esa última tarjeta de trivial de tal manera que:
1. Al jugador se le presentan 6 preguntas, previo nombre de la categoría (Geografía, Arte, Espectáculos, Historia, Ciencias y Deportes). Estas preguntas se mostrarán continuamente hasta que el jugador responda a todas, hasta que gane o hasta que pierda.
2. El jugador elige una de las preguntas y escribe la respuesta. Si acierta se suma 1 punto al marcador. Sino, no se suma ningún punto. En cualquier caso, la pregunta no vuelve a aparecer en la lista.
3. Cuando el jugador ha respondido 4 preguntas bien no hace falta que le preguntemos más porque habrá adivinado. Si el jugador ha respondido 3 preguntas mal no hace falta que preguntemos más porque habrá perdido.

Crea el programa de la manera más eficiente posible, creando todas las funciones que sean necesarias y procurando no escribir código javascript en el fichero html.
Solo obtendrán el certificado de eficiencia aquellos Sandkills que lo merezcan.*/
var respuestas = [0,0,0,0,0,0];//vector de respuestas
var preguntas=[//array de preguntas por categoria
    ["¿A qué estado actual pertenece la Baja California?",
     "¿Cuál es el color que más abunda en los taxis londinenses?",
     "¿A qué pueblo de la antigüedad pertenecían Sidón y Biblos?",
     "¿Cuál es el color que más abunda en los taxis neoyorquinos?",
     "¿Cuál fue la ciudad egipcia más importante del helenismo?",
     "¿Qué localidad más importante de Toledo da nombre a una conocida cerámica?"],
    ["¿Cuántas mujeres nos encontramos Viviendo con Mr. Cooper?",
     "¿Por qué serie de TV recordamos a Chanquete?",
     "¿Cuál fue la primera película en que cantó Julio Iglesias?",
     "¿Quién hizo popular en radio la figura del Sr. Casamajó?",
     "¿Cuántas personas componen el grupo La Unión?",
     "¿Quién nos enseñó a cantar Libertad sin ira?"],
    ["¿Qué país fue intervenido por las tropas noteramericanas contra el general Noriega?",
     "¿Bajo qué gran dinastía se contruyó la Gran Muralla China?",
     "¿Qué país oriental tuvo como religión originaria el sintoísmo?",
     "¿Cómo se llamó la ruta abierta hacia el año 110 a través de Asia Central?",
     "¿En qué guerra murió el primer soldado de color defenciendo la bandera de Estados Unidos?",
     "¿De qué color era realmente el caballo de Santiago?"],
    ["¿La Estanquera de Vallecas fue antes novela, obra de teatro o película?",
     "¿De que color ajeno al arcoiris es el obelisco de Salmanasar?",
     "¿Cuál era el libro sagrado de los antiguos Persas?",
     "¿En qué palacio se fundó la Academia de Música francesa?",
     "¿Quién fue el escultor murciano que modeló un nacimiento de barro de 556 piezas?",
     "¿Cuál es la parte del Templo de Salomón que todavía se conserva?"],
    ["¿Cuál es el resultado de multiplicar la unidad por sí misma?",
     "¿Con qué lloras cuando la cortas?",
     "¿En qué material están hechos los Toros de Guisando?",
     "¿Cómo se llaman los testigos principales de una ceremonia?",
     "¿Qué consiguen los budistas mediante el abandono de todas las pasiones?",
     "¿Qué compañía comercializó el entorno gráfico Windows?"],
    ["¿Que rally ganó Hubert Oriol tanto en coches como en motos?",
     "¿Por qué razón fue encarcelado Cassius Clay cuando era campeón mundial de boxeo?",
     "¿En qué prueba atlética fue desposeido Ben Johnson del récord del mundo y del título olímpico?",
     "¿Quién fue el primer brasileño que le costó al Barcelona más dinero que Ronaldo?",
     "¿Cuántas etapas en línea ganó Miguel Indurain en la historia del Tour de Francia?",
     "¿Cuántos atletas disputan una final olímpica de velocidad en natación?"]
];
var respuestasCorrectas=[//array de respuestas correctas por categoria
    ["A Méjico","El negro","Al fenicio","El amarillo","Alejandría","Talavera"],
    ["Tres","Por Verano Azul","La vida sigue igual","Javier Sardá","Tres","Jarcha"],
    ["Panamá","Bajo la de los Ming","Japón","Ruta de la seda","En la Segunda Guerra Mundial","Blanco"],
    ["Obra de teatro","Negro","El avesta","En el de Versalles","Francisco Salzillo","El muro de las lamentaciones"],
    ["Uno","Con la cebolla","En piedra","Padrinos","El nirvana","Microsoft"],
    ["El París Dakar","Por no querer ir a Vietnam","En cien metros lisos","Anderson","Ninguna","Ocho"]
];
var categoria = 0, pregunta = 0, puntos = 0, errores = 0;//variables necesarias(categoria y pregunta actuales,aciertos(en puntos) y errores)
function mostrarCategorias(){//texto del menu de categorias
    return  "1 - Geografía\n" +
            "2 - Espectáculos\n" +
            "3 - Historia\n" +
            "4 - Arte\n" +
            "5 - Ciencias\n" +
            "6 - Deportes\n";
}
function mostrarPreguntas(){//menu de preguntas disponibles
    var texto = "";
    for (var i=0;i<6;i++){
        if (respuestas[i]==0){//solo si la pregunta no esta contestada
            texto += (i+1) + " - " + preguntas[categoria-1][i] + "\n";//mostramos la pregunta correspondiente
        }
    }
    return texto + "Puntos = " + puntos + "\n";//tambien mostramos los puntos que lleva
}
function trivial(){
    do{
        categoria = prompt(mostrarCategorias()+"Elija una categoría");//preguntamos por la categoria
    }while(categoria > 6 || categoria < 1)//miestras no elija una correcta
    do{
        pregunta = prompt(mostrarPreguntas() + "Elija una pregunta");//preguntamos por la pregunta que quiere contestar
        if (respuestas[pregunta-1] == 0){//si la pregunta esta por contestar
            //mostramos la pregunta y pedimos la respuesta, recogiendo el valor y transformandolo para el vector de respuestas
            respuestas[pregunta-1] = (prompt(preguntas[categoria-1][pregunta-1] +
                    "\nEscriba su respuesta:").toUpperCase() == respuestasCorrectas[categoria-1][pregunta-1].toUpperCase()) ? 1 : -1;
            if (respuestas[pregunta-1] == 1){//segun el resultado sera punto o error
                puntos++;
            }else{
                errores++;
            }
        }
    }while(puntos < 4 && errores < 3)//repetidmos el proceso hasta que gane con 4 puntos o pierda con 3 errores
    if (puntos >= 4){//un mensaje no esta de mas
        alert("Enhorabuena!! has ganado");
    }else{
        alert("Has perdido, animo la próxima será");
    }

}
