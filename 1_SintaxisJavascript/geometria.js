var areaTriangulo = function (b,a){
    return (b*a)/2;
};
var perimetroTrianguloEquilatero = function (l){
    return l*3;
};
var perimetroTrianguloIsosceles = function (l1, l2){
    return l1*2+l2;
};
var perimetroTrianguloEscaleno = function (l1,l2,l3){
    return l1+l2+l3;
};
var areaCuadrado = new Function ("l","return l*l;");
var perimetroCuadrado = new Function ("l","return l*4;");
var areaRectangulo = new Function ("l1","l2","return l1*l2;");
//para no mover las funciones a otro fichero he mantenido la llamada a las funciones e insertado dentro las propias funciones automaticas
function areaCirculo(r){
    return (function (){return Math.PI*Math.pow(r,2);}());
}
function longitudCircunferencia(r){
    return (function (){return 2*r*Math.PI;}());
}
//u1e7
function perimetroTriangulo(){
    switch(arguments.length){
        case 1: return perimetroTrianguloEquilatero(arguments[0]);break;
        case 2: return perimetroTrianguloIsosceles(arguments[0],arguments[1]);break;
        case 3: return perimetroTrianguloEscaleno(arguments[0],arguments[1],arguments[2]);break;
        default: return undefined;
    }
}
var perimetroRectangulo = new Function ("l1","l2","return l1*2+l2*2;");//como no existia le creo
function perimetroParalelogramo(){
    switch(arguments.length){
        case 1: return perimetroCuadrado(arguments[0]);break;
        case 2: return perimetroRectangulo(arguments[0],arguments[1]); break;
        default: return undefined;
    }
}
function areaPoligono(lados){
    switch(lados){
        case 1: return areaCirculo(arguments[1]);break;
        case 3: return areaTriangulo(arguments[1],arguments[2]);break;
        case 4:
            if (arguments.length==2){
                return areaCuadrado(arguments[1]);break;
            }else if (arguments.length==3){
                return areaRectangulo(arguments[1],arguments[2]);break;
            }
        default: return undefined;
    }
}
