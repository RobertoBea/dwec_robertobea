var base=2, altura=3, lado1=2, lado2=3, lado3=3.605551275463989, radio=2;
function test(){
    alert("El area del triangulo con base "+base+" y altura "+altura+" es: "+areaTriangulo (base, altura));
    alert("El perimetro del triangulo equilatero con lado "+lado1+" es: "+perimetroTrianguloEquilatero(lado1));
    alert("El perimetro del triangulo isosceles con lado1 "+lado1+" y lado2 "+lado2+" es: "+perimetroTrianguloIsosceles(lado1, lado2));
    alert("El perimetro del triangulo escaleno con lados "+lado1+", "+lado2+" y "+lado3+" es: "+perimetroTrianguloEscaleno(lado1, lado2, lado3));
    alert("El area del cuadrado con lado "+lado1+" es: "+areaCuadrado(lado1));
    alert("El perimetro del cuadrado con lado "+lado1+" es: "+perimetroCuadrado(lado1));
    alert("El area del rectangulo con base "+lado1+" y altura "+lado2+" es: "+areaRectangulo(lado1, lado2));
    alert("El area del circulo con radio "+radio+" es: "+areaCirculo(radio));
    alert("La longitud de la circunferencia con radio "+radio+" es: "+longitudCircunferencia(radio));
}
function test_u1e7(){

    alert("El perimetro del triangulo equilatero con lado "+lado1+" es: "+perimetroTriangulo(lado1));
    alert("El perimetro del triangulo isosceles con lado1 "+lado1+" y lado2 "+lado2+" es: "+perimetroTriangulo(lado1, lado2));
    alert("El perimetro del triangulo escaleno con lados "+lado1+", "+lado2+" y "+lado3+" es: "+perimetroTriangulo(lado1, lado2, lado3));

    alert("El perimetro del cuadrado con lado "+lado1+" es: "+perimetroParalelogramo(lado1));
    alert("El perimetro del rectangulo con base "+lado1+" y altura "+lado2+" es: "+perimetroParalelogramo(lado1,lado2));

    alert("El area del circulo con radio "+radio+" es: "+areaPoligono(1,radio));
    alert("El area del triangulo con base "+base+" y altura "+altura+" es: "+areaPoligono (3,base, altura));
    alert("El area del cuadrado con lado "+lado1+" es: "+areaPoligono(4,lado1));
    alert("El area del rectangulo con base "+lado1+" y altura "+lado2+" es: "+areaPoligono(4,lado1, lado2));
}
