function companyia (Nombre, Responsable) {//constructor base
        //propiedades
        this.nombre = Nombre;
        this.responsable = Responsable;

        //getters
        this.getNombre = function() { return this.nombre;};
        this.getResponsable = function() { return this.responsable;};

        //setters
        this.setNombre = function(Nombre) { this.nombre = Nombre;};
        this.setResponsable = function(Responsable) { this.responsable = Responsable;};

        //toString
        this.mostrar = function(separador) {
            return "Nombre: " + this.getNombre() +
                " " + separador + "Responsable: " + this.getResponsable();
        };
    }
