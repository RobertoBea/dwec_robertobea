function escuela (Nombre, Localidad, Responsable) {//constructor base
        //propiedades
        this.nombre = Nombre;
        this.localidad = Localidad;
        this.responsable = Responsable;
        this.instalaciones = [];
        //getters
        this.getNombre = function() { return this.nombre;};
        this.getLocalidad = function() { return this.localidad;};
        this.getResponsable = function() { return this.responsable;};
        this.getInstalaciones = function() { return this.instalaciones;};
        this.getInstalacion = function(id) { return this.instalaciones[id];};

        //setters
        this.setNombre = function(Nombre) { this.nombre = Nombre;};
        this.setLocalidad = function(Localidad) { this.localidad = Localidad;};
        this.setResponsable = function(Responsable) { this.responsable = Responsable;};

        //funciones
        this.addInstalacion = function(Instalacion) {
            //buscamos que no exista una de este tipo y la insertamos si no existe
            //devolvemos true si se ha insertado y false si no puede ser
            if (this.instalaciones.length > 2) return false;
            if (buscarPor(this.instalaciones, "tipo", Instalacion.getTipo()) != undefined) return false;
            insertarAlFinal(this.instalaciones, Instalacion);
            return true;
        };
        this.removeInstalacion = function(id) {
            //si existe el id indicado lo eliminamos
            if (buscarEn(this.instalaciones, id) != undefined) this.instalaciones.splice(id, 1);
            return true;
        };

        this.listarInstalaciones = function(separador) {
            //devuelve la lista de instalaciones que tiene en formato texto
            var lista = "";
            if (this.instalaciones.length>0){
                for (var i = 0; i< this.instalaciones.length; i++){
                    lista += separador + this.instalaciones[i].mostrar(separador) + " ";
                }
            }
            return lista;
        }

        //toString
        this.mostrar = function(separador) {
            return "Nombre: " + this.getNombre() +
                " " + separador + "Localidad: " + this.getLocalidad() +
                " " + separador + "Responsable: " + this.getResponsable() +
                ((this.getInstalaciones().length > 0) ? " " + separador + "Instalaciones Eléctricas: " + this.listarInstalaciones(separador) : "");
        };

    }
