var ventanaPopUp;

function u2e1() {
    /*Diseña un programa que solicite a un usuario en una sola petición su nombre,
    dos apellidos y año de nacimiento
    Por ejemplo: "Pepe Perez Sanchez 1990".
    El nombre y los apellidos serán una única palabra cada uno,
    pero no hace falta que lo valides.
    No habrá "Juan Tomás" o "Miguel Angel".
    Sobre ese nombre deberá mostrar la siguiente información para crear un nombre de usuario:

    Parte A:
    - Tamaño de la cadena que contiene, únicamente, nombre y dos apellidos.
    Parte B:
    - Número de vocales que contiene la cadena de nombre y apellidos.
    Parte C:
    - Transformación de la cadena a todo mayúsculas.
    - Muestra de nombre, apellido1, apellido2 y año, cada cuál en una línea.
    - Nombre de usuario: estará formado por la inicial del nombre, el primer apellido,
    la inicial del segundo apellido y las dos últimas cifras del año de nacimiento,
    todo en minúsculas (ej. pperezs90).*/

    //primero solicitamos los datos y en el mismo momento los separamos para almacenarlos en un array
    //de momento no se solicita nada especial por lo que no creo un objeto que almacene dicha informacion
    //ni se realiza un control de errores

    //para la tercera parte se reparan los procesos, primero obtenemos la cadena en mayusculas y despues la separamos
    var cadena = prompt("Indique su nombre, apellidos y año de nacimiento separados por espacios").toUpperCase();
    var datos = cadena.split(" ");
    //despues se informa de la longitud de las cadenas nombre y apellidos
    //creando una cadena en el mismo alert concatenando los datos
    alert("La longitud de las cadenas es:\n" +
          "Nombre: " + datos[0].length + "\n" +
          "Apellido 1: " + datos[1].length + "\n" +
          "Apellido 2: " + datos[2].length + "\n");
    //para la segunda parte creamos una funcion que calcule las vocales de una palabra
    alert("El numero de vocales es:\n" +
          "Nombre: " + numeroVocales(datos[0]) + "\n" +
          "Apellido 1: " + numeroVocales(datos[1]) + "\n" +
          "Apellido 2: " + numeroVocales(datos[2]) + "\n");
    //para la tercera entrega añadimos una variable en la que generaremos el nombre de usuario
    var usuario = datos[0].substring(0,1) +
        datos[1] +
        datos[2].substring(0,1) +
        datos[3].substr(2,2);//como sabemos que es un año con 4 caracteres podemos determinar directamente la posicion de inicio
    alert("Nombre: " + datos[0] + "\n" +
          "Apellido 1: " + datos[1] + "\n" +
          "Apellido 2: " + datos[2] + "\n" +
          "Año de nacimiento: " + datos[3] + "\n" +
          "Usuario: " + usuario + "\n");
}
function numeroVocales(cadena) {
    var vocales = 0;
    cadena = cadena.toUpperCase();
    for (var i=0;i<cadena.length;i++){
        if("AEIOU".indexOf(cadena.charAt(i))>-1) vocales++;
    }
    return vocales;
}

function u2e2() {
    /*Crea un conversor de bases que solicite al usuario en qué base está el número que va a introducir
    (binario, decimal, octal, hexadecimal) y muestre el número en las 4 bases.
    Trata de hacerlo de la manera más optimizada posible.*/
    //lo primero obtenemos los parametros, la base y el numero
    var base = prompt("Indica la basse del numero");
    var numero = prompt("Indica el numero");
    var decimal = parseInt("" + numero, base);//convertimos el numero a decimal
    var binario = decimal.toString(2);//obtenemos los valores en binario octal y hexadecimal
    var octal = decimal.toString(8);
    var hexadecimal = decimal.toString(16);
    //por ultimo mostramos los valores
    alert("Binario: " + binario + "\n" +
         "Octal: " + octal + "\n" +
         "Decimal: " + decimal + "\n" +
         "Hexadecimal: " + hexadecimal);
}

function u2e3() {
    /*Realiza un programa que permita elegir al usuario un tipo de lotería del estado y genere los números aleatorios  en la propia página.
    Se mostrarán los siguientes tipos de loterías:
    Lotería nacional (los ceros a la izquierda deben mostrarse).
    Quiniela.
    Primitiva (con complementario y reintegro).*/
    var juego;
    do{//solicitamos el juego e impedimos que elioja otras opciones
    juego = prompt("Indique el tipo de juego:\n" +
                      "1 - Lotería Nacional\n" +
                      "2 - Quiniela\n" +
                      "3 - Primitiva");
    }while(juego > "3" || juego < "1");
    var numero;//declaramos la variable que recogera los numeros
    var resultado = "";//para guardar conjuntos de resultados
    switch(juego){
        case "1": //loteria 00000-99999
            //obtenemos el numero y le sumamos 100000 para tener todos los ceros
            //luego recogemos solo la parte a partir del segundo numero
            //he creado una funcion para el calculo de numeros aleatorios
            resultado = llenarConCeros(aleatorio(0,99999),5);
            break;
        case "2": //quiniela 1x2 *15
            for (var i = 0; i < 15; i++){
                numero = aleatorio(0,2);
                resultado += ((numero < 1) ? "X" : numero) + "<br>";//sustituimos los 0 por X
            }
            break;
        case "3": //primitiva 1-49 *6 +reintegro(0-9)
            for (var i = 0; i < 6; i++){
                do{//evitamos que salga el mismo numero
                    numero = aleatorio(1,49);
                }while(resultado.indexOf(" " + numero + "<") > -1)
                resultado += " " + numero + "<br>";
                //acumulamos los numeros con espacios para diferenciarlos mejor en la cadena
            }
            resultado += "Reintegro: " + aleatorio(0,9);
            break;
    }
    document.getElementById("numeros").innerHTML = resultado;
}
function aleatorio(desde, hasta){
    return Math.round((Math.random()*hasta)+desde);
}
function llenarConCeros(numero, digitos){
    return (numero + Math.pow(10, digitos)).toString().substring(1);
}

function u2e4a() {
    /*Realiza un programa que sea capaz de calcular cuántas veces cae en sábado el solsticio de verano desde el año 2000 hasta el año 2100. */
    var fecha;
    var cuantosSolsticiosEnSabado = 0;
    for (var i = 2000; i < 2101; i++){
        //suponiendo que el solsticio de verano es el 21 de junio
        //recorremos todos los años, creamos la fecha y
        //averiguamos si el dia de la semana es sabado
        fecha = new Date(i, 5, 21);//5 porque los meses empieza en 0
        if (fecha.getDay() == 6) cuantosSolsticiosEnSabado++;
    }
    alert("Solsticios de verano desde 2000 a 2100: " + cuantosSolsticiosEnSabado);
}

function u2e4b() {
    /*Realiza un archivo en js con tres funciones que te permitan realizar los siguientes formatos de fecha recibiendo como parámetro la fecha en milisegundos o en números (3 parámetros) -utiliza arguments para saber el número de parámetros que ha introducido el usuario-:*/
    //llamamos a las funciones pasando fechas u horas,
    //ajuntamos los meses a lo que seria un mes de un usuario
    var f = new Date();
    alert(fechaCorta(123456789123));
    alert(fechaCorta(f.getFullYear(),f.getMonth()+1,f.getDate()));
    alert(fechaLarga(123456789123));
    alert(fechaLarga(f.getFullYear(),f.getMonth()+1,f.getDate()));
    alert(fechaIngles(123456789123));
    alert(fechaIngles(f.getFullYear(),f.getMonth()+1,f.getDate()));

    alert(horaCorto(123456789123));
    alert(horaCorto(f.getHours(),f.getMinutes(),f.getSeconds()));
    alert(horaLargo(123456789123));
    alert(horaLargo(f.getHours(),f.getMinutes(),f.getSeconds()));
    alert(horaPMAM(123456789123));
    alert(horaPMAM(f.getHours(),f.getMinutes(),f.getSeconds()));
}

function abrirVentanaPopUp(){
    //hemos creado la pagina a la que llamamos con unas dimensiones de prueba
    ventanaPopUp = window.open("ventanaPopUp.html","","width=500,height=50");
}
function cerrarVentanaPopUp(){
    ventanaPopUp.close();
}
function imprimirVentanaPopUp(){
    ventanaPopUp.print();
}
function cerrarVentanaActual(){
    self.close();
}
function moverVentanaActual(){
    var posicion;
    do{//solicitamos las coordenadas nuevas
        posicion = prompt("indique la posicion separada por una coma").split(",");
    }while (posicion.length != 2)
    self.moveTo(posicion[0],posicion[1]);

}
//para u3e5 windowTiempo
//creamos las variables necesarias, el intervalo del reloj,
// los millisegundos de inicio del crono y si esta pausado el tiempo
var hora, inicioCrono = 0, pausa = false;
function u3e5(){
    hora = setInterval(actualizaHora,1);
}
function actualizaHora() {//obtenemos la hora y la mostramos
    var f = new Date();
    document.getElementById("hora").innerHTML = "Hora: " + horaLargo(f.getTime());

    //mostramos la hora del crono mediante una funcion que lo calcula
    if (inicioCrono > 0 && !pausa) { //si esta iniciado y no hay pausa
         document.getElementById("crono").innerHTML = "Crono: " + tiempoTranscurrido(inicioCrono);
    }
}
function crono0() {//limpiamos el intervalo para que no muestra mas y actualizamos el visor
    inicioCrono = 0;
    document.getElementById("crono").innerHTML = "Crono: 0";
}
function cronoInicio() {//creamos el intervalo del crono guardamos la hora actual y nos aseguramos que no haya pausa
    inicioCrono =  new Date().getTime();
    pausa = false;
}
function cronoPausa() {//pausamos
    pausa = true;
}
function cronoGuardar() {
    document.getElementById("tiempos").innerHTML += document.getElementById("crono").innerHTML.substring(7) + "<br>";
}


function u3e1() {
    /*Crea tres objetos que te permitan almacenar información de sandkills.
Deberás realizarlos de la siguiente manera.
sandkill1: creado como un literal.
sandkill2: creado con la definición de Object (utilizando new).
sandkill3: primero crearás un constructor de objeto llamado Sandkill y a partir de él te crearás el objeto sandkill3.
Los  tres objetos tendrá las mismas propiedades: nombre, edad, especialidad.
Y un método .mostrar() que devuelva una cadena con la información de cada sandkill en una sola línea.
Posteriormente añade a sandkill1 la propiedad "nacionalidad" y a sandkill2 la propiedad "lenguajeFavorito".
Por último borra de sandkill3 la propiedad "especialidad".
Crea  una función que, pasado un objeto de tipo Sandkill, muestre todas sus propiedades, pero sin llamar a su método mostrar.
Llama a la función tres veces para mostrar la información de los tres objetos creados.
Modifica en sandkill1 el nombre, en sandkill2 el lenguajeFavorito y en sandkill3 la edad.
Vuelve a llamar a la función de mostrar las propiedades de los tres objetos.*/
    var sandkill1 = {nombre:"Pepe", edad:"50", especialidad:"redes",
            mostrar: function(){
            return "Nombre: " + this.nombre + " Edad: " + this.edad +
                     " Especialidad: " + this.especialidad;}};
    var sandkill2 = new Object();
    sandkill2.nombre = "Jorge";
    sandkill2.edad = 25;
    sandkill2.especialidad = "diseño";
    sandkill2.mostrar = function() {
            return "Nombre: " + this.nombre + " Edad: " + this.edad +
                     " Especialidad: " + this.especialidad;};
    function sandkill (Nombre, Edad, Especialidad) {
        this.nombre = Nombre;
        this.edad = Edad;
        this.especialidad = Especialidad;
        this.mostrar = function(){
            return "Nombre: " + this.nombre + " Edad: " + this.edad +
                     " Especialidad: " + this.especialidad;}
    }
    var sandkill3 = new sandkill("Roberto", 37, "programacion");

    sandkill1.nacionalidad = "Española";
    sandkill2.lenguajeFavorito = "C#";
    delete sandkill3.especialidad;

    mostrarDatosSandKill(sandkill1);
    mostrarDatosSandKill(sandkill2);
    mostrarDatosSandKill(sandkill3);
    sandkill1.nombre += " Juan";
    sandkill2.lenguajeFavorito = "C++";
    sandkill3.edad = 32;
    mostrarDatosSandKill(sandkill1);
    mostrarDatosSandKill(sandkill2);
    mostrarDatosSandKill(sandkill3);
}
function mostrarDatosSandKill(sk){
    var sand = document.getElementById("sandKills");
    for (dato in sk){
        sand.innerHTML += dato + " " + sk[dato] + "<br>";
    }
}


function u3e2() {
    /*Crea un buen prototipo para almacenar información de Sandkills. Los objetos de este tipo tendrán la siguiente información:
- Nombre.
- Edad.
- Especialidad: almacenará un número (1, 2, 3).
- Compañero: almacenará un objeto de tipo Sandkill.
Además dispondrá de los siguientes métodos:
- mostrar: devuelve una cadena con la información del Sandkill nombre, edad, especialidad y nombre del compañero.
¡Ojo! Para mostrar esta información utilizarás los métodos get de cada propiedad.
- getNombre: devuelve el nombre del sandkill.
- getEdad: devuelve la edad del sandkill.
- getEspecialidad: devuelve una cadena con la especialidad a partir del número indicado.
Si es 1, devuelve Sistemas; si es 2 devuelve Web; si es 3 devuelve Multiplataforma.
- getNombreCompanero: devuelve una cadena con el nombre del compañero (getNombre).
- getCompanero: devuelve un objeto de tipo Sandkill.
- setNombre: modifica el nombre del sandkill.
- setEdad: modifica la edad del sandkill.
- setEspecialidad: modifica la especialidad del sandkill (recibe un número).
- setEspecialidadNombre: recibe una cadena y almacena un número en función de si la cadena recibida es Sistemas (1), Web (2) o Multiplataforma(3).
- setCompanero: recibe un objeto de tipo Sandkill y lo almacena.*/

    //para probar
    var sandkillCompanero = new Sandkill("Jose", 23, 1, null);
    var sandkill1 = new Sandkill("Luis", 30, 2, sandkillCompanero);
    alert(sandkill1.mostrar());

}
function Sandkill (Nombre, Edad, Especialidad, Companero) {//constructor base
        //propiedades
        this.nombre = Nombre;
        this.edad = Edad;
        this.especialidad = Especialidad;
        this.companero = Companero;
        //getters
        this.getNombre = function() { return this.nombre;};
        this.getEdad = function() { return this.edad;};
        this.getEspecialidad = function() {
            switch (this.especialidad){
                case 1:
                    return "Sistemas";
                    break;
                case 2:
                    return "Web";
                    break;
                case 3:
                    return "Multiplataforma";
                    break;
                default:
                    return null;
                    break;
            }
        };
        this.getNombreCompanero = function() { return ((this.companero != null) ? this.companero.getNombre() : "");};
        this.getCompanero = function() { return this.companero;};
        //setters
        this.setNombre = function(Nombre) { this.nombre = Nombre;};
        this.setEdad = function(Edad) { this.edad = Edad;};
        this.setEspecialidad = function(Especialidad) {
            this.especialidad = Especialidad;
        };
        this.setEspecialidadNombre = function(Especialidad) {
            switch (Especialidad){
                case "Sistemas":
                    this.especialidad = 1;
                    break;
                case "Web":
                    this.especialidad = 2;
                    break;
                case "Multiplataforma":
                    this.especialidad = 3;
                    break;
                default:
                    this.especialidad = null;
                    break;
            }
        };
        this.setCompanero = function(Companero) { this.companero = Companero;};
        //toString
        this.mostrar = function() {
            return "Nombre: " + this.getNombre() +
                " Edad: " + this.getEdad() +
                " Especialidad: " + this.getEspecialidad() +
                " Compañero: " + this.getNombreCompanero();
        };

    };


function u3e3() {
    /*El siguiente programa necesitará dos archivos en javascript.
El primero contendrá el prototipo Sandkill creado en el ejercicio anterior.
El segundo permitirá trabajar con arrays de cualquier tipo de dato.
Diseña un programa que permita realizas las siguientes operaciones:
- Insertar un sandkill en una lista al principio.
- Insertar un sandkill en una lista al final.
- Borrar el primer sandkill de la lista.
- Borrar el último sandkill de la lista.
- Mostrar la lista de sandkills.
- Mostrar la lista de sandkills ordenada.
- Buscar un sandkill a partir de su nombre.
- Buscar un sandkill a partir de su posición.

¡¡MUY IMPORTANTE!! Hazlo de tal manera que puedas reutilizar el archivo de gestión de arrays
y te funcione con cualquier tipo de objeto que crees, por tanto trabajarás con variables,
 independientemente del tipo que sean.
(Por ejemplo: function ordenarLista (lista); o function buscarElemento(posicion);*/

    //para probar las propiedades
    var sandkills = [];
    alert(sandkills.length);
    insertarAlInicio(sandkills, new Sandkill("Jose",20,3,null));
    insertarAlFinal(sandkills, new Sandkill("Ana",35,3,null));
    insertarAlInicio(sandkills, new Sandkill("Jose1",20,3,null));
    insertarAlFinal(sandkills, new Sandkill("Ana1",35,3,null));
    insertarAlInicio(sandkills, new Sandkill("Jose2",20,3,null));
    insertarAlFinal(sandkills, new Sandkill("Ana2",35,3,null));
    insertarAlInicio(sandkills, new Sandkill("Jose3",20,3,null));
    insertarAlFinal(sandkills, new Sandkill("Ana3",35,3,null));
    alert(sandkills.length);
    quitarDelInicio(sandkills);
    quitarDelFinal(sandkills);
    alert(sandkills.length);
    alert(mostrarLista(sandkills));
    ordenarLista(sandkills, "nombre");
    alert(mostrarLista(sandkills));
    var sandkill1 = buscarPor(sandkills, "nombre", "Jose2");
    var sandkill2 = buscarEn(sandkills, 4);
    alert(sandkill1.mostrar());
    alert(sandkill2.mostrar());

}





