function insertarAlInicio(lista, dato){//inserta al principio
    return lista.unshift(dato);
}
function insertarAlFinal(lista, dato){//inserta al final
    return lista.push(dato);
}
function quitarDelInicio(lista){//quita del principio y lo devuelve si lo quieres
    return lista.shift();
}
function quitarDelFinal(lista){//quita del final y devuelve si lo quieres
    return lista.pop();
}
function mostrarLista(lista){//muestra el contenido de la lista en formato string
    var resultado = "";
    for (var i = 0; i < lista.length; i++){
        resultado += ((arguments.length > 0) ? (i + 1) + " -> " : "") +
            lista[i].mostrar() + ((arguments.length > 0) ? arguments[1] : "\n");
    }
    return resultado;
}
function ordenarLista(lista){//ordena la lista a partir del campo indicado, si lo hay sino la ordena de forma normal
    if (arguments.length == 1){
        lista.sort();
    }else{
        var orden = arguments[1];
        lista.sort(function(a,b){return (a[orden] < b[orden]) ? -1 : ((a[orden] > b[orden]) ? 1 : 0);});
    }
}
function buscarPor(lista, propiedad, valor){
    if (arguments.length > 3 && arguments[3] == true){//le he añadido una funcionalidad para que devuelva todos los que encuentre
        return lista.filter(function(o) { return o[propiedad] == valor; });
    }else{//en este caso solo devolvera la primera aparicion
        return lista.filter(function(o) { return o[propiedad] == valor; })[0];
    }
}
function buscarEn(lista, posicion) {
    return lista[posicion];
}
