function fechaCorta(){//- Fecha en formato corto: 17/02/2017
    var fecha = getFechaFromArgs(arguments);
    if (fecha == null) return fecha;
    return fecha.toLocaleDateString();
}
function fechaLarga(){//- Fecha en formato largo: Miércoles, 17 de febrero de 2017.
    var fecha = getFechaFromArgs(arguments);
    if (fecha == null) return fecha;
    var dia = ["Domingo",
                 "Lunes",
                 "Martes",
                 "Miercoles",
                 "Jueves",
                 "Viernes",
                 "Sabado"];
    var mes = ["Enero",
                 "Febrero",
                 "Marzo",
                 "Abril",
                 "Mayo",
                 "Junio",
                 "Julio",
                 "Agosto",
                 "Septiembre",
                 "Octubre",
                 "Noviembre",
                 "Deciembre"];
    return dia[fecha.getDay()] + ", " + fecha.getDate() + " de " +
            mes[fecha.getMonth()] + " de " + fecha.getFullYear();
}
function fechaIngles(){//- Fecha en formato inglés: Wednesday, February 17, 2017.
    var fecha = getFechaFromArgs(arguments);
    if (fecha == null) return fecha;
    var dia = ["Sunday",
                 "Monday",
                 "Tuesday",
                 "Wednesday",
                 "Thursday",
                 "Friday",
                 "Saturday"];
    var mes = ["January",
                 "February",
                 "March",
                 "April",
                 "May",
                 "June",
                 "July",
                 "August",
                 "September",
                 "October",
                 "November",
                 "December"];
    return dia[fecha.getDay()] + ", " + mes[fecha.getMonth()] + " " +
            fecha.getDate() + ", " + fecha.getFullYear();
}
function horaCorto(){//- Hora en formato corto: 14:30.
    var hora = getFechaFromArgs(arguments);
    if (hora == null) return hora;
    return llenarConCeros(hora.getHours(),2) + ":" +
        llenarConCeros(hora.getMinutes(),2);
}
function horaLargo(){//- Hora en formato largo: 14:30:25.
    var hora = getFechaFromArgs(arguments);
    if (hora == null) return hora;
    return llenarConCeros(hora.getHours(),2) + ":" +
        llenarConCeros(hora.getMinutes(),2) + ":" +
        llenarConCeros(hora.getSeconds(),2);
}
function horaPMAM(){
    /*- Hora con PM/AM: 02:30 AM si es antes de medio día o 02:30 PM si es después del mediodía.
    No se utilizan horas mayores que 12 ni menores que 1 (no existe 00:30).*/
    var hora = getFechaFromArgs(arguments);
    if (hora == null) return hora;
    var h = hora.getHours(), tipo = "AM";
    if (h == 0) h = 24;
    if (h > 12){
        h -= 12;
        tipo = "PM";
    }
    return llenarConCeros(h,2) + ":" +
        llenarConCeros(hora.getMinutes(),2) + " " + tipo;
}
function tiempoTranscurrido(milisegundos){//restamos de la hora actual la hora de inicio pasada
    var f = new Date(new Date().getTime() - milisegundos);
    return llenarConCeros(f.getHours() -1, 2) + ":" +
            llenarConCeros(f.getMinutes(), 2) + ":" +
            llenarConCeros(f.getSeconds(), 2) + "." +
            llenarConCeros(f.getMilliseconds(), 3);
}
function getFechaFromArgs(args){
    switch (args.length){
        case 0:
            return null;
            break;
        case 1:
            return new Date(args[0]);
            break;
        default:
            if(args[0] > 24){//con fecha
                return new Date(args[0],args[1],args[2]);
            }else{//con hora
                return new Date(1,0,1,args[0],args[1],args[2]);
            }
            break;
    }

}
