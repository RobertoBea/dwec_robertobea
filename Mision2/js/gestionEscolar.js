//variables para almacenar los distintos objetos
var escuelas = [];
var instalaciones = [];
var companyias = [];
var html;//para almacenar los htmls
//menus para la creacion de las pantallas y visualizacion de los objetos
function menu(obj){//segun la coleccion adaptamos el menu
    var coleccion = eval(obj.toLowerCase());
    html = "<br>" + ((obj=="Companyias")?"Compañias":obj) + "<br>";
    html += "<br><button onclick='nueva(\"" + obj + "\")'>Nueva</button><br><br>Lista:<br><br>";
    if (coleccion.length > 0){
        for (var i = 0; i < coleccion.length; i++){
            html += "<button onclick='ver(\"" + obj + "\"," + i + ")'>Ver</button>" +
                "<button onclick='editar(\"" + obj + "\"," + i + ")'>Editar</button>" +
                "<button onclick='borrar(\"" + obj + "\"," + i + ")'>Borrar</button>" +
                "<br>" + (i + 1) + " -> " + coleccion[i].mostrar("<br>") + "<br>";
        }
    }
    document.getElementById("contenido").innerHTML = html;
}
//creacion
function nueva(obj){// creamos un formulario personalizado segun la coleccion
    html = "<form id = 'formulario'>";
    switch(obj){
        case "Escuelas"://con nombre,localidad y responsable
            html += "Nombre:<br>" +
                  "<input id='n0' type='text' value=''>" +
                  "<br>" +
                  "Localidad:<br>" +
                  "<input id='n1'  type='text' value=''>" +
                  "<br>" +
                  "Responsable:<br>" +
                  "<input id='n2'  type='text' value=''>" +
                  "<br>";
            break;
        case "Instalaciones"://con tipo(radio), cantidad y presupuesto
            html += "Cantidad:<br>" +
                  "<form id = 'n0' action=''>" +
                  "<input type='radio' name='tipo' value='Exterior'> Exterior" +
                  "<input type='radio' name='tipo' value='Interior'> Interior" +
                  "<input type='radio' name='tipo' value='Enchufes'> Enchufes" +
                  "</form>" +
                  "Cantidad:<br>" +
                  "<input id='n1' type='text' value=''>" +
                  "<br>" +
                  "Presupuesto:<br>" +
                  "<input id='n2'  type='text' value=''>" +
                  "<br>";
            break;
        case "Companyias"://con nombre y responsable
            html += "Nombre:<br>" +
                  "<input id='n0' type='text' value=''>" +
                  "<br>" +
                  "Responsable:<br>" +
                  "<input id='n1'  type='text' value=''>" +
                  "<br>";
            break;
    }
    //y los botones
    html += "<button onclick='validar(\"" + obj + "\")'>Crear</button>" +
            "<input type='reset' value='Limpiar'>" +
        "</form>";
    document.getElementById("contenido").innerHTML = html;
}
//visualizacion pasamos el nombre de la coleccion y el id
function ver(obj, id){
    document.getElementById("contenido").innerHTML = "<br>" +
                eval(obj.toLowerCase() + "[" + id + "].mostrar('<br>')");
}
//modificacion con nombre de coleccion e id
function editar(obj, id){
    var coleccion = eval(obj.toLowerCase());
    switch(obj){
        case "Escuelas"://con nombre,localidad y responsable
            html = "Nombre:<br>" +
                  "<input id='n0' type='text' value='" + coleccion[id].getNombre() + "'>" +
                  "<br>" +
                  "Localidad:<br>" +
                  "<input id='n1'  type='text' value='" + coleccion[id].getLocalidad() + "'>" +
                  "<br>" +
                  "Responsable:<br>" +
                  "<input id='n2'  type='text' value='" + coleccion[id].getResponsable() + "'>" +
                  "<br>" + ((coleccion[id].getInstalaciones().length >2) ? "" :
                "<button onclick='addInstalacion(" + id + ")'>Añadir instalación</button><br>") +
                ((coleccion[id].getInstalaciones().length == 0) ? "" :
                "<button onclick='removeInstalacion(" + id + ")'>Quitar instalación</button><br>") + "<br>";
            break;
        case "Instalaciones"://con tipo(radio), cantidad y presupuesto
            var tipo = coleccion[id].getTipo();
            html = "Cantidad:<br>" +
                "<form id = 'n0' action=''>" +
                  "<input type='radio' name='tipo' value='Exterior' " +
                ((tipo == "Exterior") ? "checked" : "") + "> Exterior" +
                  "<input type='radio' name='tipo' value='Interior' " +
                ((tipo == "Interior") ? "checked" : "") + "> Interior" +
                  "<input type='radio' name='tipo' value='Enchufes' " +
                ((tipo == "Enchufes") ? "checked" : "") + "> Enchufes" +
                "</form>" +
                "Cantidad:<br>" +
                "<input id='n1' type='text' value='" + coleccion[id].getCantidad() + "'>" +
                "<br>" +
                "Presupuesto:<br>" +
                "<input id='n2'  type='text' value='" + coleccion[id].getPresupuesto() + "'>" +
                "<br>" +
                "<button onclick='elegirCompanyia(" + id + ")'>Elegir compañía</button><br><br>";
            break;
        case "Companyias"://con nombre y responsable
            html = "Nombre:<br>" +
                  "<input id='n0' type='text' value='" + coleccion[id].getNombre() + "'>" +
                  "<br>" +
                  "Responsable:<br>" +
                  "<input id='n1'  type='text' value='" + coleccion[id].getResponsable() + "'>" +
                  "<br>";
            break;
    }
     //y los botones, en la validacion se pasa el id para actualizar el registro
    html += "<button onclick='validar(\"" + obj + "\"," + id + ")'>Guardar</button>" +
            "<input type='reset' value='Limpiar'>" +
        "</form>";
    document.getElementById("contenido").innerHTML = html;
}
//borrado
function borrar(obj, id){//con la coleccion y el id
    eval(obj.toLowerCase() + ".splice(" + id + ", 1)");//borramos el elemento
    eval("menu('" + obj + "')");//recargamos el menu
}
//validacion
function validar(obj){//segun coleccion
    //obtenemos los valores
    var n0, n1, n2;//vara recoger los valores
    var nombre, valores;//nombre de los prototipos y lista de valores para crear los objetos
    switch(obj){
        case "Escuelas"://con nombre,localidad y responsable
            nombre = "escuela";
            n0 = document.getElementById("n0").value;
            n1 = document.getElementById("n1").value;
            n2 = document.getElementById("n2").value;
            valores = n0 + "','" + n1 + "','" + n2;
            break;
        case "Instalaciones"://con tipo(radio), cantidad y presupuesto
            nombre = "instalacion";
            n0 = document.querySelector('input[name="tipo"]:checked').value;//obtenemos el checked
            n1 = document.getElementById("n1").value;
            n2 = document.getElementById("n2").value;
            valores = n0 + "','" + n1 + "','" + n2;
            break;
        case "Companyias"://con nombre y responsable
            nombre = "companyia";
            n0 = document.getElementById("n0").value;
            n1 = document.getElementById("n1").value;
            n2 = "x"; //para que nos acepte como valido
            valores = n0 + "','" + n1;
            break;
    }
    //si tienen valor todos creamos la escuela
    if (n0.trim() != "" && n1.trim() != "" && n2.trim() != ""){
        var obj1;
        if (arguments.length > 1){
            var coleccion = eval(obj.toLowerCase());
            obj1 = coleccion[arguments[1]];
            switch(obj){
                case "Escuelas"://con nombre,localidad y responsable
                    obj1.setNombre(n0);
                    obj1.setLocalidad(n1);
                    obj1.setResponsable(n2);
                    break;
                case "Instalaciones"://con tipo(radio), cantidad y presupuesto
                    obj1.setTipo(n0);
                    obj1.setCantidad(n1);
                    obj1.setPresupuesto(n2);
                    break;
                case "Companyias"://con nombre y responsable
                    obj1.setNombre(n0);
                    obj1.setResponsable(n1);
                    break;
            }
        }else{
            obj1 = eval("new " + nombre + "('" + valores + "')");
            insertarAlFinal(eval(obj.toLowerCase()), obj1);
        }
        menu(obj);//recargamos el menu
    } else{//si faltan valores
        //si no hay mensaje, mostramos un  mensaje de error
        html = document.getElementById("contenido").innerHTML;
        if (html.indexOf("<p>") < 0) html += "<br><p style='color:red;'>Rellene todos los campos</p>";
        document.getElementById("contenido").innerHTML = html;
    }
}
//funciones especificas
function addInstalacion(id){//añade de las instalaciones disponibles una a la escuela
    var idInstalacion;
    do { //mostramos la lista de las instalacines disponibles numeradas
        idInstalacion = prompt(mostrarLista(instalaciones, "\n") + "0 -> Cancelar \n Indique la instalación que quiere añadir");
    }while (idInstalacion < 0 || idInstalacion > instalaciones.length);
    if (idInstalacion > 0) {
        if (!escuelas[id].addInstalacion(instalaciones[idInstalacion - 1])){
            alert("Ya existe ese tipo de instalación");
        }
    }
}
function removeInstalacion(id){//quita una instalación de la escuela
    var idInstalacion;
    do { //mostramos la lista de las instalacines disponibles numeradas
        idInstalacion = prompt(mostrarLista(escuelas[id].getInstalaciones(), "\n") + "0 -> Cancelar \n Indique la instalación que quiere quitar");
    }while (idInstalacion < 0 || idInstalacion > escuelas[id].getInstalaciones().length);
    if (idInstalacion > 0) escuelas[id].removeInstalacion(idInstalacion - 1);
}
function elegirCompanyia(id){//establece la compañia que tiene la instalacion
    var idCompanyia;
    do { //mostramos la lista de las instalacines disponibles numeradas
        idCompanyia = prompt(mostrarLista(companyias, "\n") + "0 -> Cancelar \n Indique la companyia que quiere añadir");
    }while (idCompanyia < 0 || idCompanyia > companyias.length);
    if (idCompanyia > 0) instalaciones[id].setCompanyia(companyias[idCompanyia - 1]);
}
