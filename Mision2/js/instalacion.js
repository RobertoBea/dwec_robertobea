function instalacion (Tipo, Cantidad, Presupuesto) {//constructor base
        //propiedades
        this.tipo = Tipo;
        this.cantidad = Cantidad;
        this.presupuesto = Presupuesto;
        this.companyia;
        //getters
        this.getTipo = function() { return this.tipo;};
        this.getCantidad = function() { return this.cantidad;};
        this.getPresupuesto = function() { return this.presupuesto;};
        this.getCompanyia = function() { return this.companyia;};

        //setters
        this.setTipo = function(Tipo) { this.tipo = Tipo;};
        this.setCantidad = function(Cantidad) { this.cantidad = Cantidad;};
        this.setPresupuesto = function(Presupuesto) { this.presupuesto = Presupuesto;};
        this.setCompanyia = function(Companyia) { this.companyia = Companyia;};

        //toString
        this.mostrar = function(separador) {
            return "Tipo: " + this.getTipo() +
                " " + separador + "Cantidad: " + this.getCantidad() +
                " " + separador + "Presupuesto: " + this.getPresupuesto() +
                ((this.getCompanyia()==undefined)?"":" " + separador + "Compañia Eléctrica: " + this.getCompanyia().getNombre());
        };
    }
