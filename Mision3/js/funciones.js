var cartas = [], partidas = -1, puntos = 0, actual = 0;
function nuevaPartida(){
    partidas++;
    document.getElementById("partidas").innerHTML="Partidas: " + partidas;
    puntos += actual;
    actual = 0;
    document.getElementById("puntos").innerHTML="Puntos: " + puntos;
    for (var i = 1; i  < 11; i++){//resetamos las cartas mostrando la parte de la imagen donde esta la caratula'
        document.getElementById("c" + i).style.backgroundPosition = "-16px -25px";
        cartas[i] = aleatorio(1, 13);//guardamos las cartas
    }
}
function mostrarCarta(id){//cambiamos la posicion de la imagen para que muestre la imagen que queremos
    document.getElementById("c" + id).style.backgroundPosition = "-" + ((cartas[id] * 91) + 16) + "px -25px";//posicion inicial + 91 que ocupara cada carta
    document.getElementById("c" + id).style.border = "solid red 1px"//aplicamos un colorete
    setTimeout(checkPartida, 100);//verificamos el estado de la partida actual con un retraso para que se vea la carta
}
function checkPartida(){
    var suma = 0;
    for (var i = 1; i  < 11; i++){//resetamos las cartas mostrando la parte de la imagen donde esta la caratula'
        if (document.getElementById("c" + i).style.backgroundPosition != "-16px -25px") suma += cartas[i]; // si no tienen la posicion inicial es que les hemos dado la vuelta
    }
    if (suma < 17) suma = 16;//para delimitar los resultados
    if (suma > 21) suma = 22;
    switch (suma) {
        case 16: actual = 0; break;//sin puntos todavia
        case 17: actual = 1; break;//con puntos
        case 18: actual = 2; break;
        case 19: actual = 4; break;
        case 20: actual = 7; break;
        case 21: actual = 10; alert("Enhorabuena conseguiste 21"); nuevaPartida(); break;//consigues 21, nueva partida
        case 22: actual = 0; alert("Lo siento no lo conseguiste"); nuevaPartida(); break;//te has pasado, nueva partida
    }
}
function aleatorio(desde, hasta){
    return Math.round(Math.floor(Math.random()*hasta)+desde);
}
