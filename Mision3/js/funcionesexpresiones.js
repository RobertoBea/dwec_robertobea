window.onload = iniciar;

function iniciar(){
    //establecemos los eventos
    document.getElementById("btnfecha").addEventListener("click", validar, false);
    document.getElementById("btncocinero").addEventListener("click", validar, false);
    document.getElementById("btndestinatario").addEventListener("click", validar, false);
    document.getElementById("btngramos").addEventListener("click", validar, false);
    document.getElementById("btncomposicion").addEventListener("click", validar, false);
}
function validar(e){
    //segun  el boton tendremos una expresion regular u otra y el id del textarea
    var id = e.currentTarget.id.substring(3);
    switch(id){
        case "fecha"://"dd/mm/aaaa hh:mm"
            valida(id, /^(0[1-9]|[1-2]\d|3[01])(\/)(0[1-9]|1[012])\2(\d{4}) ([0-1][0-9]|2[0-3]):[0-5][0-9]$/);
            break;
        case "cocinero"://(ej. WW$1234)
            valida(id,/^[A-Z]{2}[^a-zA-Z0-9]\d{4}$/);
            break;
        case "destinatario"://(ej. NM_alburquerque:1234)
            valida(id,/^[A-Z]{2,3}_[a-z]+:\d{4}$/);
            break;
        case "gramos"://1000 al 5000
            valida(id,/^([1-4][0-9]{3}|5000)$/);
            break;
        case "composicion"://(ej. 200gC3OH7)
            valida(id,/^[1-9][0-9]*g([A-Za-z]{1,2}[0-9]?){2}$/);
            break;
    }
}
function valida(id, expresion){
    //recibimos el nombre de el textarea a validar y la expresion regular a usar
    var texto = document.getElementById(id).value;//recogemos el texto
    var img = document.getElementById("img" + id);//recogemos el img
    if (expresion.test(texto)){//asignamos la imagen segun el caso
        img.src = "img/ok.png";
    }else{
        img.src = "img/nook.png";
    }
}
