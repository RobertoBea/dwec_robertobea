window.onload = iniciar;
var intervalo, err;

function iniciar(){
    //establecemos los eventos
    document.getElementById("enviara").addEventListener("click", autenticar, false);
    document.getElementById("enviarb").addEventListener("click", validar, false);
    document.getElementById("firma").addEventListener("focus", firmar, false);
    document.addEventListener("keydown", reiniciaIntervalo , false);//para los avisos de Jessie
    if (getCookie("Pase") != undefined){
        mostrarFormulariob();
    }
    intervalo = setInterval(avisoJessie,15000);//avisos cada 15 segundos
}

// A) AUTENTICAR
function autenticar(e){
    var usuario = document.getElementById("usuario");
    var password = document.getElementById("password");
    if (usuario.value == "RobertoBea" && password.value == "CeedCV"){
        setCookie("Pase","Ok");
        mostrarFormulariob();
    }else{
        var aut = document.getElementById("autenticacion");
        aut.style.color = "red";
        aut.innerHTML = "Acceso denegado";
        e.preventDefault();
    }
    return false;
}
function mostrarFormulariob(){//escondemos el formulario de autenticar y mostramos el del virus
    document.getElementById("formularioa").style.display = "none";
    document.getElementById("formulariob").style.display = "initial";
}
// A) + COOKIES
function getCookie(clave) { //funcion para obtener el valor de la cookie, si no existe devuelve undefined
    var ca = document.cookie.split('; ');
    return ca.find(function (cookie) {
        return cookie.indexOf(clave) === 0;
    });
}
function setCookie(clave, valor) { //funcion para guardar cookies
  var fecha = new Date();
  var caduca = fecha.getTime() + 36000000;//le aplico una caducidad de una hora popr probar
  fecha.setTime(caduca);
  document.cookie = clave + "=" + valor + "; expires=" + fecha.toGMTString() + ";";//en el formato adecuado GMT
}

// B) VALIDAR VIRUS
function validar(e){
    err = false;
    //validamos todos los campos
    valida("nombre");
    valida("numero");
    valida("idfirma");
    valida("firma");

    if (err){
        e.preventDefault();//impedimos el envio
        return false;//impedimos limpiar los campos
    }else{
        return true;
    }
}
function valida(id){
    var elemento = document.getElementById(id);//obtenemos el elemento
    if(!elemento.checkValidity()){//verificamos que la validacion sea correcta
        var el = elemento.nextSibling;//establecemos el color y texto de la etiqueta de eror
        el.style.color = "red";
        el.innerHTML = " " + elemento.validationMessage + " " + elemento.title;
        elemento.className="error";//asignamos el estilo de borde rojo
        elemento.focus();//ponemos el foco en el ultimo error
        err = true;
    }else{
        elemento.nextSibling.innerHTML="";//vaciamos el error
    elemento.className="";
    }
}
function firmar(e){ //requisito para rellenar la firma en caso de tener menos de 8 caracteres
    if (e.currentTarget.value.length < 8) {
        e.currentTarget.value = "S4ND1EG0";
    }
}

// C) JESSIE
function avisoJessie(){
    alert("Ponte a trabajar, no seas Jessie Pinkman");
}
function reiniciaIntervalo(){
    clearInterval(intervalo);//limpiamos el intervalo viejo
    intervalo = setInterval(avisoJessie,15000);//avisos cada 15 segundos
}
