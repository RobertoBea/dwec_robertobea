window.onload = iniciar;
var err;//verificacion de errores
function iniciar(){
    //establecemos los eventos
    document.getElementById("enviar").addEventListener("click", validar, false);
    document.getElementById("club").addEventListener("click", setClub, false);
}
function setClub(club){
    //activamos los campos relacionados conb el club
    var activo = club.currentTarget.checked;
    document.getElementById("socio").disabled = !activo;
    var radios = document.getElementsByName("categoria");//todos los radios de la categoria
    for (var i = 0; i<radios.length;i++){
        radios[i].disabled=!activo;
    }
}
function validar(e){
    err = false;
    //validamos todos los campos
    valida("identificador");
    valida("nombre");
    valida("fecha");
    valida("email");
    valida("telefono");
    valida("nombre");
    valida("edad");
    valida("socio");

    if (err){
        e.preventDefault();//impedimos el envio
        return false;//impedimos limpiar los campos
    }else{
        return true;
    }
}
function valida(id){
    var elemento = document.getElementById(id);//obtenemos el elemento
    if(!elemento.checkValidity()){//verificamos que la validacion sea correcta
        var el = elemento.nextSibling;//establecemos el color y texto de la etiqueta de eror
        el.style.color = "red";
        el.innerHTML = " " + elemento.validationMessage + " " + elemento.title;
        elemento.className="error";//asignamos el estilo de borde rojo
        elemento.focus();//ponemos el foco en el ultimo error
        err = true;
    }else{
        elemento.nextSibling.innerHTML="";//vaciamos el error
    elemento.className="";
    }
}
