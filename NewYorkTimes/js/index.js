$(document).ready(function () {
            $("#buscar").on("click", function (e) {
                //obtenemos los valores
                var busqueda = $("#busqueda").val();
                var inicio = $("#fechaInicio").val();
                var fin = $("#fechaFin").val();
                var orden = $("#orden").val();
                //realizamos verificaciones
                if (busqueda.trim() == "") {//que haya algo en la busqueda
                    alert("Indique una busqueda");
                } else if (inicio.trim() == "" || !isDate(inicio)) {//fecha correcta y rellena
                    alert("La fecha de inicio no es correcta");
                } else if (fin.trim() == "" || !isDate(fin)) {//fecha correcta y rellena
                    alert("La fecha de fin no es correcta");
                } else if (inicio>fin) {//fecha inicio< que fecha fin
                    alert("La fecha de inicio no puede ser mayor que la de fin");
                } else {//si va todo bien
                    var url = "https://api.nytimes.com/svc/search/v2/articlesearch.json";
                    url += '?' + $.param({
                      'api-key': "a49757eca88c4130ac519affb0d2d661",
                      'q': busqueda,
                      'begin_date': inicio,
                      'end_date': fin,
                      'sort': orden
                    });
                    $.ajax({
                      url: url,
                      method: 'GET',
                    }).done(function(result) {
                        //vaciamos la lista
                        var noticias = $("#noticias").empty();
                        $.each(result.response.docs,function(index, value){
                            //por cada documento he decidido crear un enlace con el snippet que nos lleva a la pagina de la noticia aunque se podrian poner mas datos
                            noticias.append("<li><a href='"+value.web_url+"'>"+ value.snippet +"</a></li>")
                        });
                    }).fail(function(err) {
                      alert(err.message);//mostramos el mensaje de error
                    });
                }
                e.preventDefault();
                return true;
            });

        });

        function isDate(fecha) {
            //aunque no hacia falta, he añadido un metodo para comprovar la validez de la fecha
            var d = new Date();
            d.setFullYear(parseInt(fecha.substr(0,4)), parseInt(fecha.substr(4,2))-1, parseInt(fecha.substr(6,2)));
            if(isNaN(d)){
                return false;
            } else if(d.getFullYear()!=parseInt(fecha.substr(0,4)) ||
                     (d.getMonth()+1)!=parseInt(fecha.substr(4,2)) ||
                     d.getDate()!=parseInt(fecha.substr(6,2)) ){
                return false;
            }
           return true;
        }
