function articulo() {
    this.id;
    this.cat;
    this.nombre;
    this.unidades;
    this.precio;
}
$(document).ready(function () {
    $("#vaciarCarrito").on("click", vaciarCarrito);
    $("#recargarTienda").on("click", recargarTienda);

    if (typeof (Storage) == "undefined") {
        // no soporta webstorage
        alert("No soportado web storage");
    } else {
        //localStorage.removeItem("Tienda");
        if (localStorage.Tienda) { //hay tienda
            mostrar("Tienda");
        } else { //no hay tienda
            recargarTienda();
        }
    }

});

function mostrar(data) {
    //visual
    var articulos = JSON.parse(localStorage.getItem(data));
    //vacio lo que hay
    $("#" + data).empty();
    $("#" + data).append("<tr><th>Id</th><th>Cat</th><th>Nombre</th><th>Ud</th><th>Precio</th></tr>");
    //relleno
    if (articulos) {
        //ordenamos por categoria
        articulos.sort(function (o, d) {
            return o.cat > d.cat;
        });
        for (var i in articulos) {
            pasaA(data, i, articulos[i]);
        };
    }
    if (data == "Tienda") mostrar("Carrito");
}

function recargarTienda() {
    //vaciado y recargado local
    localStorage.removeItem("Tienda");
    var url = "http://hispabyte.net/DWEC/entregable2-2.php";
    $.ajax({
        url: url,
        method: 'GET',
    }).done(function (data) {
        //guardo en local
        localStorage.Tienda = JSON.stringify(JSON.parse(data));
        //muestro visual
        mostrar("Tienda");
    }).fail(function (err) {
        alert(err.message); //mostramos el mensaje de error
    });
    vaciarCarrito();
}

function vaciarCarrito() {
    //datos locales
    localStorage.removeItem("Carrito");
    //visual
    $("#Carrito").empty();
    $("#Carrito").append("<tr><th>Id</th><th>Cat</th><th>Nombre</th><th>Ud</th><th>Precio</th></tr>");
}

function pasaA(a, id, art) {
    $("#" + a).append("<tr class='articulo" + a + "'>" +
        "<td id='" + a + "Id" + id + "'>" + art.id + "</td>" +
        "<td id='" + a + "Categoria" + id + "'>" + art.cat + "</td>" +
        "<td id='" + a + "Nombre" + id + "'>" + art.nombre + "</td>" +
        "<td id='" + a + "Unidades" + id + "'>" + art.unidades + "</td>" +
        "<td id='" + a + "Precio" + id + "'>" + art.precio + "</td>" +
        ((a == "Tienda") ? "<td><input id='" + a + "Input" + id + "' type='text' style='width:100px'/><button onclick='inserta(" + id + ");'>Al carro</button></td>" : "") +
        "</tr>");
}

function creaArticulo(de, id) {
    var art = new articulo();
    art.id = $("#" + de + "Id" + id).text();
    art.cat = $("#" + de + "Categoria" + id).text();
    art.nombre = $("#" + de + "Nombre" + id).text();
    art.unidades = ((de == "Tienda") ? $("#" + de + "Input" + id).val() : $("#" + de + "Unidades" + id).text());
    art.precio = $("#" + de + "Precio" + id).text();
    return art;
}

function inserta(id) {
    if (isNaN($("#" + de + "Input" + id).val())) {
        alert("Indique una cantidad numérica");
    } else {
        var art = creaArticulo("Tienda", id);
        pasaA("Carrito", id, art);
        guardarCarrito(art);
    }
}

function guardarCarrito(art) {
    var articulos;
    if (localStorage.Carrito) {
        articulos = JSON.parse(localStorage.Carrito);
    } else {
        articulos = [];
    }
    articulos.push(art);
    localStorage.Carrito = JSON.stringify(articulos);
}
